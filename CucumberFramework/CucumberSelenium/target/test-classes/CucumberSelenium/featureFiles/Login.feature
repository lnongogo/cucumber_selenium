Feature: Login into Github account
		Existing user should be able to login with correct credentials

Scenario: Login into Github account with correct credentials
		Given User navigates to the Github website
		And User clicks the Login button
		And User enters the correct username
		And User enters the correct password
		When User clicks the Sign in button
		Then User should be redirected to dashboard 