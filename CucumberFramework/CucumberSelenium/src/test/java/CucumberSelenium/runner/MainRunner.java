package CucumberSelenium.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions(features = { "src/test/java/CucumberSelenium/featureFiles/" }, glue = {
		"CucumberSelenium.stepFiles"}, monochrome= true,
				plugin = {"pretty"}

)

public class MainRunner {

}
