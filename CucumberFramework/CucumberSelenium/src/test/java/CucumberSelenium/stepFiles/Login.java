package CucumberSelenium.stepFiles;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.firefox.FirefoxDriver;


import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class Login {
	WebDriver driver;
	
	
	//Setting browser properties
	@Before 
	public void setup() {
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\lnongogo\\Desktop\\CucumberFramework\\CucumberSelenium\\WebDrivers\\geckodriver.exe");
		this.driver = new FirefoxDriver();
		this.driver.manage().window().maximize();
		this.driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		this.driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
	}
	
	@After
	public void tearDown() throws InterruptedException {
		Thread.sleep(2000);;
		this.driver.manage().deleteAllCookies();
		this.driver.quit();
		this.driver =null;
	}
	
	@Given("^User navigates to the Github website$")
	public void user_navigates_to_the_Github_website() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   driver.get("https://github.com/");
	   System.out.println("Successfully navigated to the Github website");
	}

	@And("^User clicks the Login button$")
	public void user_clicks_the_Login_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.xpath("//a[@href='/login']")).click();
		System.out.println("Clicked the Login - {Sign-In} button successfully");
	}

	@And("^User enters the correct username$")
	public void user_enters_the_correct_username() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.xpath("//input[@id='login_field']"));
		driver.findElement(By.xpath("//input[@id='login_field']")).sendKeys("LonwaboTest");
		
		System.out.println("Entered username successfully");
		
	}

	@And("^User enters the correct password$")
	public void user_enters_the_correct_password() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	
		driver.findElement(By.xpath("//input[@id='password']"));
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys("1502Lloyd");
		System.out.println("Entered password successfully");
	}

	@When("^User clicks the Sign in button$")
	public void user_clicks_the_Sign_in_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.xpath("//input[@value='Sign in']"));
		driver.findElement(By.xpath("//input[@value='Sign in']")).click();
		System.out.println("Successfully clicked the Sign In button");
		
	}

	@Then("^User should be redirected to dashboard$")
	public void user_should_be_redirected_to_dashboard() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.xpath("//body[@class='logged-in env-production page-responsive min-width-0 page-dashboard full-width']"));

		Thread.sleep(2000);
		//WebElement pullRequestLink = driver.findElement(By.xpath("//a[contains(text(), 'Pull requests')]"));
		
		
		
		System.out.println("Successfully logged into Github");
	}
}
