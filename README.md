# Cucumber_Selenium
Selenium embedded in Cucumber Framework

**Prerequisites**

  * This project needs ECLIPSE - JAVA to be installed on the operating machine, preferrable the latest version or **Eclipse IDE for Java Developers Version: 2018-09 (4.9.0)**
  
  * User system variables Java_Home need to be set correctly, for referrence go to: https://docs.oracle.com/cd/E19182-01/821-0917/6nluh6gq9/index.html 
  
  
  
 # Installing
    "Once the **Prerequisites** are met, clone the project to the desktop."
    * Launch the Eclipse IDE.
    * Upon successfully launching the IDE, Select the directory of the cloned project.
    * Click "Launch" and wait for workbench to be loaded.
    * Under "Package Explorer", there will be a folder named "CucumberSelenium".
    * Right-click on the folder.
    * Find the option "Maven".
    * Under the "Maven" option, look for "Update Project".
    * The project will update and install all the required dependencies. (These can be found in the **POM.xml** file).
          **With the projet expanded, go to the JRE System Library package.
          **Set the JRE properties by Right-Clicking on the JRE System Library package.
          **Look for the "Properties" option.
          **Click on properties.
          **Click the radio button next to "Workspace default JRE".
          **Click "Apply and Close"
    * Click the arrow next to "CucumberSelenium" in order to expand the project folder.
    * Proceed to look for the directory "src/test/java"
    * Click the arrow to expand the Folder.
    * There will be a folder written "CucumberSelenium" once more.
    * Under the **featureFiles** package folder, a **Login.feature** file contains the BDD (Behavioural-Driven Development) steps.
    * Under the **runner** package folder, a **MainRunner.java** file contains the code to run the project. This code is of Java language.
    * Under the **stepFiles** package folder, a **Login.java** file contains the code that translates the BDD steps to Java Selenium steps.
    
    
 # Running the test
 
 * Double-click the **MainRunner.java** the code (Optional).
 * Right-click the **MainRunner.java** the code (Optional).
 * Go to "Run As".
 * Click the **J1 Unit Test** option.
 
 
 
 # Author: **Lonwabo Nongogo
 
